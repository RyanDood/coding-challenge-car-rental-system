--CREATING DATABASE:
create database Car_Rental_System

--CREATING TABLES:
create table Vehicle(
vehicleID int primary key,
make varchar(50),
model varchar(50),
year int,
dailyRate decimal(8,2),
status int,
passengerCapacity int,
engineCapacity int
)


create table Customer(
customerID int primary key,
firstName varchar(50),
lastName varchar(50),
email varchar(50),
phoneNumber varchar(50),
)

create table Lease(
leaseID int primary key,
vehicleID int,
customerID int,
startDate date,
endDate date,
type varchar(50)
constraint Lease_Vehicle foreign key(vehicleID) references Vehicle(vehicleID) on delete cascade,
constraint Lease_Customer foreign key(customerID) references Customer(customerID) on delete cascade
)

create table Payment(
paymentID int primary key,
leaseID int,
paymentDate date,
amount decimal(8,2),
constraint Payment_Lease foreign key(leaseID) references Lease(leaseID) on delete cascade,
)

alter table Payment
add constraint IX_LeaseID
unique(leaseID)

--INSERTING VALUES:
insert into Vehicle
values
(1,'Toyota','Camry',2022,50.00,1,4,1450),
(2,'Honda','Civic',2023,45.00,1,7,1500),
(3,'Ford','Focus',2022,48.00,0,4,1400),
(4,'Nissan','Altima',2023,52.00,1,7,1200),
(5,'Chevrolet','Malibu',2022,47.00,1,4,1800),
(6,'Hyundai','Sonata',2023,49.00,0,7,1400),
(7,'BMW','3 Series',2023,60.00,1,7,2499),
(8,'Mercedes','C-Class',2022,58.00,1,8,2599),
(9,'Audi','A4',2022,55.00,0,4,2500),
(10,'Lexus','ES',2023,54.00,1,4,2500)

insert into Customer
values
(1,'John','Doe','johndoe@example.com','555-555-5555'),
(2,'Jane','Smith','janesmith@example.com','555-123-4567'),
(3,'Robert','Johnson','robert@example.com','555-789-1234'),
(4,'Sarah','Brown','sarah@example.com','555-456-7890'),
(5,'David','Lee','david@example.com','555-987-6543'),
(6,'Laura','Hall','laura@example.com','555-234-5678'),
(7,'Michael','Davis','michael@example.com',' 555-876-5432'),
(8,'Emma','Wilson','emma@example.com','555-432-1098'),
(9,'William','Taylor','william@example.com','555-321-6547'),
(10,'Olivia','Adams','olivia@example.com','555-765-4321')

insert into Lease
values
(1,1,1,'2023-01-01','2023-01-05','Daily'),
(2,2,2,'2023-02-15','2023-02-28','Monthly'),
(3,3,3,'2023-03-10','2023-03-15','Daily'),
(4,4,4,'2023-04-20','2023-04-30','Monthly'),
(5,5,5,'2023-05-05','2023-05-10','Daily'),
(6,4,3,'2023-06-15','2023-06-30','Monthly'),
(7,7,7,'2023-07-01','2023-07-10','Daily'),
(8,8,8,'2023-08-12','2023-08-15','Monthly'),
(9,3,3,'2023-09-07','2023-09-10','Daily'),
(10,10,10,'2023-10-10','2023-10-31','Monthly')

insert into Payment
values
(1,1,'2023-01-03',200.00),
(2,2,'2023-02-20',1000.00),
(3,3,'2023-03-12',75.00),
(4,4,'2023-04-25',900.00),
(5,5,'2023-05-07',60.00),
(6,6,'2023-06-18',1200.00),
(7,7,'2023-07-03',40.00),
(8,8,'2023-08-14',1100.00),
(9,9,'2023-09-09',80.00),
(10,10,'2023-10-25',1500.00)

--QUERIES:
--1. Update the daily rate for a Mercedes car to 68.

update Vehicle
set dailyRate = 68.00
where make = 'Mercedes'

select * from Vehicle

--2. Delete a specific customer and all associated leases and payments.

delete from Customer
where customerID = 10

--3. Rename the "paymentDate" column in the Payment table to "transactionDate".

exec sp_rename 'Payment.paymentDate', 'transactionDate'  

--4. Find a specific customer by email.

select * from Customer
where email = 'johndoe@example.com'

--5. Get active leases for a specific customer.

update Lease
set endDate = '2023-12-25'
where leaseID = 9

select * from Lease
where endDate >= (select convert(date, GETDATE()))

--6. Find all payments made by a customer with a specific phone number.

select firstName,phoneNumber,paymentID,transactionDate,amount from Payment inner join Lease
on Payment.leaseID = Lease.leaseID inner join Customer
on Lease.customerID = Customer.customerID
where phoneNumber = '555-789-1234'

--7. Calculate the average daily rate of all available cars.

select avg(dailyRate) as avg_daily_rate from Vehicle

--8. Find the car with the highest daily rate.

select make,model,dailyRate from Vehicle
where dailyRate = (select max(dailyRate) from Vehicle)

--9. Retrieve all cars leased by a specific customer.

select Lease.customerID,firstName,make,model from Lease inner join Vehicle
on Lease.vehicleID = Vehicle.vehicleID inner join Customer
on Lease.customerID = Customer.customerID
where Lease.customerID = 3
group by Lease.customerID,firstName,make,model

--10. Find the details of the most recent lease.

select top 1 * from Lease
order by leaseID desc

--11. List all payments made in the year 2023.

update Payment
set transactionDate = '2022-12-31'
where leaseID = 1

select DATENAME(YEAR,GETDATE())

select * from Payment inner join (select paymentID,DATENAME(YEAR,transactionDate) as payment_year from Payment) P
on Payment.paymentID = P.paymentID
where payment_year = '2023'

--12. Retrieve customers who have not made any payments.

select Customer.customerID,firstName,phoneNumber from Payment inner join Lease
on Payment.leaseID = Lease.leaseID right join Customer
on Lease.customerID = Customer.customerID
where paymentID is null

--13. Retrieve Car Details and Their Total Payments.

select Vehicle.vehicleID,make,model,sum(amount) as total_payments from Payment inner join Lease
on Payment.leaseID = Lease.leaseID inner join Vehicle
on Lease.vehicleID = Vehicle.vehicleID
group by Vehicle.vehicleID,make,model

--14. Calculate Total Payments for Each Customer.

select Customer.customerID,firstName,phoneNumber,sum(amount) as total_payments from Payment inner join Lease
on Payment.leaseID = Lease.leaseID inner join Customer
on Lease.customerID = Customer.customerID
group by Customer.customerID,firstName,phoneNumber

--15. List Car Details for Each Lease.

select leaseID,Vehicle.vehicleID,make,model,dailyRate from Lease inner join Vehicle
on Lease.vehicleID = Vehicle.vehicleID

--16. Retrieve Details of Active Leases with Customer and Car Information.

select firstName,phoneNumber,make,model,startDate,endDate from Lease inner join Customer
on Lease.customerID = Customer.customerID inner join Vehicle
on Lease.vehicleID = Vehicle.vehicleID
where endDate >= (select convert(date, GETDATE()))

--17. Find the Customer Who Has Spent the Most on Leases.

select firstName,phoneNumber,total_payments from Customer inner join (select Customer.customerID,sum(amount) as total_payments from Payment inner join Lease
on Payment.leaseID = Lease.leaseID inner join Customer
on Lease.customerID = Customer.customerID
group by Customer.customerID,firstName,phoneNumber) P
on Customer.customerID = P.customerID
where total_payments = (select max(total_payments) from Customer inner join (select Customer.customerID,sum(amount) as total_payments from Payment inner join Lease
on Payment.leaseID = Lease.leaseID inner join Customer
on Lease.customerID = Customer.customerID
group by Customer.customerID,firstName,phoneNumber) P
on Customer.customerID = P.customerID)


--18. List All Cars with Their Current Lease Information

select Vehicle.vehicleID,make,model,startDate,endDate,type from Lease right join Vehicle
on Lease.vehicleID = Vehicle.vehicleID